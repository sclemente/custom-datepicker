(function(angular) {
	/* globals com */

	/**
	 * @ngdoc directive
	 * @name component.customDatepicker
	 * @description
	 * Componente encargado del manejo del datepicker
	 */
	angular
        .module('customDatepicker', [])
		.component('customDatepicker', {
			templateUrl: function($element, $attrs) {
				var _init = $attrs.initializer; //Cogemos el initializer para averiguar si es un calendario o un rango de fechas
				_init = _init.replace(/ /g, ''); //Eliminamos espacios para poder coger el caracter que se necesita
				var _isRange = _init.charAt(_init.indexOf('isRange') + 8); //Cogemos el primer caracter del valor de isRange
				if (_isRange === 't') { //Es un rango de fechas desde-hasta
					return '../bower_components/custom-datepicker/custom-range-datepicker.html';
				} else { //Por defecto un único dp
					return '../bower_components/custom-datepicker/custom-datepicker.html';
				}
			},
			controller: customDatepickerController,
			bindings: {
				initializer: '<',
				model: '=',
				isRequired: '<',
				classes: '<'
			}
		});

	customDatepickerController.$inject = ['$translate'];

	function customDatepickerController($translate) {
		var ctrl = this;
		if (!ctrl.initializer.isRange) {
			var configDP = {
				minDate: ctrl.initializer.config.minDate,
				maxDate: ctrl.initializer.config.maxDate
			};
			dpCreation($('.fecha'), configDP);
		} else {
			var _from = $('.fechaDesde');
			var _to = $('.fechaHasta');
			var configFrom = {
				minDate: ctrl.initializer.config.minDate,
				maxDate: _to.val(),
				toDP: _to
			};
			var configTo = {
				minDate: _from.val(),
				maxDate: ctrl.initializer.config.maxDate,
				fromDP: _from
			};
			dpCreation(_from, configFrom);
			dpCreation(_to, configTo);
		}
		ctrl.placeholder = $translate.instant(ctrl.initializer.config.format.toUpperCase());
		//Aplicación de la máscara al pulsar una tecla en el input
		ctrl.setMask = function() {
			try {
				//Oculto el calendario
				$(".fecha").datepicker("hide");
				$(".fechaDesde").datepicker("hide");
				$(".fechaHasta").datepicker("hide");
				//Aplicación de la máscara
				var _length = ctrl.model.length;
				var _lastChar = ctrl.model.charCodeAt(_length - 1);
				if (!(_lastChar >= 48 && _lastChar <= 57)) {
					ctrl.model = ctrl.model.slice(0, -1);
				}
				if (_length === 3 || _length === 6) {
					if (_lastChar !== '/') {
						var _end = ctrl.model.substring(_length - 1, _length);
						var _beginning = ctrl.model.substring(0, _length - 1);
						ctrl.model = _beginning + '/' + _end;
					}
				}
			} catch (e) {}
		};

		//Comprobación de la fecha correcta de manera automática
		//De momento no es necesario hacer esta comprobación hasta el submit, así que se deja comentado, pero preparado por si en algún momento hay que volver a ponerlo.
		ctrl.checkDate = function() {
			//Meses con 31 días
			/*try {
				var _month31Days = [1, 3, 5, 7, 8, 10, 12]; //Enero-Marzo-Mayo-Julio-Agosto-Octubre-Diciembre

				//Expresiones regulares para validar el número correcto de días dependiendo del mes que corresponda
				var _day31Regex = /^(3[01]|[12][0-9]|0[1-9]|[1-9])$/;
				var _day30Regex = /^(3[0]|[12][0-9]|0[1-9]|[1-9])$/;
				var _day29Regex = /^([12][0-9]|0[1-9]|[1-9])$/;
				var _day28Regex = /^(2[0-8]|[1][0-9]|0[1-9]|[1-9])$/;
				var _monthRegex = /^(0[0-2]|[1-9]|1[1-2])$/;

				//Obtener fecha (dependiendo de la máscara)
				var _day, _month, _year;

				if (ctrl.initializer.config.format === 'dd/MM/yyyy') {
					_day = parseInt(ctrl.model.substring(0, 2));
					_month = parseInt(ctrl.model.substring(3, 5));
					_year = parseInt(ctrl.model.substring(6, 10));
				} else {
					_day = parseInt(ctrl.model.substring(3, 5));
					_month = parseInt(ctrl.model.substring(0, 2));
					_year = parseInt(ctrl.model.substring(6, 10));
				}

				//Calculo de correctos. Por defecto el día es correcto y sólo valido el mes
				var _dayOk = true;
				var _monthOk = _monthRegex.test(_month);

				if (_monthOk) { //Mes correcto
					if (_month31Days.indexOf(_month) !== -1) { //El mes tiene 31 días
						_dayOk = _day31Regex.test(_day);
					} else if (_month === 2) { //El mes es febrero
						//Compruebo si el año es bisiesto
						var _leapYear = ((_year % 4 === 0) && ((_year % 100 !== 0) || (_year % 400 === 0)));
						if (_leapYear) {
							_dayOk = _day29Regex.test(_day);
						} else {
							_dayOk = _day28Regex.test(_day);
						}
					} else { //En cualquier otro caso --> Abril-Junio-Septiembre-Noviembre
						_dayOk = _day30Regex.test(_day);
					}
				}

				//Hago la comprobación de si es correcto
				if (_monthOk && _dayOk) {
					ctrl.classes = '';
				} else {
					ctrl.classes = 'error';
				}
			} catch (e) {}*/
		};

		//Creación datepicker con las opciones apropiadas
		function dpCreation(element, options) {
			var _dateConfig = getLanguajeConfig();
			var _minDate = (options.minDate === undefined || options.minDate === null) ? null : options.minDate;
			var _maxDate = (options.maxDate === undefined || options.maxDate === null) ? null : options.maxDate;
			//Elemento jquery del datepicker "desde", para cuando el que se está creando es el "hasta"
			var _fromDP = (!!options.fromDP) ? options.fromDP : null;
			//Elemento jquery del datepicker "hasta", para cuando el que se está creando es el "desde"
			var _toDP = (!!options.toDP) ? options.toDP : null;
			//Variable para saber si los domingos son seleccionables
			var nonSundays = (!!ctrl.initializer.config.nonSundays) ? true : false;
			//Variable para saber si son seleccionables los días de fiesta
			var nonWorkingDates = (!!ctrl.initializer.config.nonWorkingDates) ? true : false;

			element.datepicker({
				dateFormat: _dateConfig.dateFormat,
				firstDay: _dateConfig.firstDay,
				monthNamesShort: _dateConfig.shortMonth,
				monthNames: _dateConfig.month,
				dayNamesMin: _dateConfig.shortDay,
				dayNamesShort: _dateConfig.shortDay,
				dayNames: _dateConfig.day,
				changeMonth: true,
				changeYear: true,
				minDate: _minDate,
				maxDate: _maxDate,
				onClose: function(selectedDate) {
					if (_fromDP !== null) {
						_fromDP.datepicker('option', 'maxDate', selectedDate);
					}
					if (_toDP !== null) {
						_toDP.datepicker('option', 'minDate', selectedDate);
					}
					//Comprobar si hay errores
					ctrl.checkDate();
				},
				showOptions: { direction: "up" }
			});
			if (nonSundays || nonWorkingDates) {
				element.datepicker('option', 'beforeShowDay', beforeShowDay);
			}
		}

		function beforeShowDay(date) {
			var nonSundays = (!!ctrl.initializer.config.nonSundays) ? true : false;
			var nonWorkingDates = (!!ctrl.initializer.config.nonWorkingDates) ? true : false;
			var businessDays = (!!ctrl.initializer.config.businessDays) ? ctrl.initializer.config.businessDays : []; ///Lista de días laborables del año en curso, en formato mm/dd/aaaa
			var show = false;
			//Inhabilitar días festivos si procede
			if (nonWorkingDates) {
				for (var i = 0; i < businessDays.length; i++) {
					var _date = new Date(businessDays[i]);
					//Si no estamos en el año en curso, dejamos seleccionar todos
					if(_date.getFullYear() !== date.getFullYear()) {
						show = true;
					}
					//Configuro las seleccionables del año en curso
					if (_date.toString() === date.toString()) { 
						show = true; 
					} 
				}
			} else { //Si no procede se habilita todo
				show = true;
			}
			//Inhabilitar domingos si procede
			if (nonSundays && date.getDay() === 0) {
				show = false;
			}
			var display = [show, ''];
			return display;
		}

		function getLanguajeConfig() {
			var _lang = ctrl.initializer.config.lang;
			var _dateFormat = ctrl.initializer.config.format.toLowerCase();
			_dateFormat = _dateFormat.replace('yyyy', 'yy');
			var _firstDay, _m, _ms, _d, _ds;
			if (_lang === 'en') {
				_firstDay = 0;
				_m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				_ms = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				_d = ['Monday', 'Tuesday', 'Wenesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
				_ds = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
			} else if (_lang === 'es') {
				_firstDay = 1;
				_m = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
				_ms = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				_d = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			} else if (_lang === 'pt') {
				_firstDay = 1;
				_m = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Decembro'];
				_ms = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dec'];
				_d = ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'];
				_ds = ['Do', 'Se', 'Te', 'Q', 'Qu', 'Se', 'Sa'];
			} else if (_lang === 'eu') {
				_firstDay = 1;
				_m = ['Urtarrila', 'Otsaila', 'Martxoa', 'Apirila', 'maiatzaren', 'Ekaina', 'Uztaila', 'Abuztua', 'Iraila', 'Urria', 'Azaroa', 'Abendua'];
				_ms = ['Urt', 'Ots', 'Mar', 'Api', 'Mai', 'Eka', 'Uzt', 'Abu', 'Ira', 'Urr', 'Aza', 'Abe'];
				_d = ['Astelehena', 'asteartea', 'asteazkena', 'osteguna', 'ostirala', 'larunbata', 'igandea'];
				_ds = ['As', 'As', 'As', 'Os', 'Os', 'La', 'Ig'];
			} else if (_lang === 'fr') {
				_firstDay = 1;
				_m = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
				_ms = ['Jan', 'Fev', 'Mar', 'Avt', 'Mai', 'Jui', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'];
				_d = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
				_ds = ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'];
			} else if (_lang === 'it') {
				_firstDay = 1;
				_m = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
				_ms = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Sey', 'Ott', 'Nov', 'Dic'];
				_d = ['Lunedi', 'Martedì', 'Mercoledì', 'Giovedi', 'Venerdì', 'Sabato', 'Domenica'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			} else if (_lang === 'de') {
				_firstDay = 1;
				_m = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
				_ms = ['Jan', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Sey', 'Ott', 'Nov', 'Dic'];
				_d = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			} else if (_lang === 'gl') {
				_firstDay = 1;
				_m = ['Xaneiro', 'Febreiro', 'Marzo', 'Abril', 'Maio', 'xuño', 'Xullo', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Decembro'];
				_ms = ['Xan', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Sey', 'Ott', 'Nov', 'Dic'];
				_d = ['Luns', 'Martes', 'Mércores', 'Xoves', 'Venres', 'Sábado', 'Domingo'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			} else if (_lang === 'ca') {
				_firstDay = 1;
				_m = ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre'];
				_ms = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Sey', 'Ott', 'Nov', 'Dic'];
				_d = ['Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte', 'Diumenge'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			} else {
				_firstDay = 1;
				_m = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
				_ms = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				_d = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
				_ds = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
			}
			var _obj = { dateFormat: _dateFormat, firstDay: _firstDay, month: _m, shortMonth: _ms, day: _d, shortDay: _ds };

			return _obj;
		}
	}

})(window.angular);

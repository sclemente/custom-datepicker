
# packaged custom-datepicker

Este es un repositorio hecho para su distribución en `bower`.

## Instalación

### bower

```shell
bower install http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/custom-datepicker.git --save
```

El `--save` es optativo pero recomendable. Si se incluye esta opción no es necesario incluir ni los estilos ni el javascript en la página principal. Si no se incluye consultar cómo incluirlos en la siguiente sección.

## Uso

### JavaScript

Añadir a los `<script>` del `index.html`:

```html
    <script src="bower_components/custom-datepicker/custom.datepicker.js"></script>
```

Añadir `customDatepicker` como dependencia al módulo del `core`:

```javascript
angular.module('app.core', [..., 'customDatepicker', ...]);
```

### Html

Para configurar el componente se utilizan diferentes atributos.

* `initializer`: Objeto de configuración general del calendario. Consta de los siguientes atributos:
	* `isRange`: Indica si es un solo calendario o es un rango de fechas desde-hasta.
	* `config`: Objeto de configuración.
		* `name`: Nombre del input que se creará.
		* `lang`: Idioma en el que se quiere mostrar el calendario - vendrá de la configuración general de la aplicación.
		* `format`: Formato de fechas que se quiere que tenga el formulario - vendrá de la configuración general de la aplicación y será dd/mm/aaaa o mm/dd/aaaa.
        * `nonSundays`: Valor booleano que indica si los domingos son seleccionables o no. Por defecto, todos los días son seleccionables, por lo que su valor es `false`.
        * `nonWorkingDates`: Valor booleano que indica si las fechas festivas son seleccionables o no. Por defecto, todos los días son seleccionables, por lo que su valor es `false`.
        * `businessDays`: Array de fechas seleccionables en el datepicker. Las fechas de este array tendrán el formato `mm/dd/aaaa`. Solo se utiliza en el caso de que el atributo `nonWorkingDates` tenga el valor `true`.
        * `minDate`: Valor que indicará la fecha mínima seleccionable, en el formato indicado por el datepicker de jquery-ui.
        * `maxDate`: Valor que indicará la fecha máxima seleccionable, en el formato indicado por el datepicker de jquery-ui.
* `model`: Modelo al que va asociado el valor de input.
* `is-required`: Valor booleano validación de campo si su valor es `true`. Si el campo es opcional, su valor será `false`.
* `classes`: Clases a aplicar en el input. Un ejemplo significativo sería la inclusión de la clase de error si ha de tenerse en cuenta.

Los atributos 
```html
    <custom-datepicker 
        initializer="{
            isRange:<valor>, 
            config: {
                name: '<nombre>', 
                lang: '<idioma>', 
                format: <formato>,
                [nonSundays: <boolean>,]
                [nonWorkingDates: <boolean>,
                businessDays: <array>,]
                [minDate: <valor>,]
                [maxDate: <valor>]
            } 
        }"
        model="<model>"
        is-required="false"
        classes="<clases>"
    ></custom-datepicker>
```

El input desde el que se abrirá el calendario:

![Html Image](readme-images/inputDP.png)

Una vez se ha abierto el calendario, su apariencia es:

![Html Image](readme-images/dpAbierto.png)

## Consideraciones adicionales

### Dependencias

Las dependencia del componente, incluida en el bower.json, es: 

```javascript
    "common-resources": "http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/common-resources.git"
```

Esta dependencia hace que el componente contenga todo el js necesario para su funcionamiento y además hace que no necesite tener ni estilos ni imágenes propias, ya que el paquete `common-resources` contiene todo lo necesario.

### Traducciones

Los literales necesarios vienen incluidos en el js del componente, por lo que no tiene archivo de internacionalización.

### A tener en cuenta

* Este componente está basado en el datepicker de jquery-ui. Para más información de configuración consultar http://jqueryui.com/datepicker/
* El placeholder que será el formato de fechas definido en la configuración del usuario. Será necesario añadir como claves en el properties de cada aplicación los literales los pares:
```javascript
{
    "DD/MM/AAAA": "dd/mm/aaaa",
    "MM/DD/AAAA": "mm/dd/aaaa"
}
```
* El input que abrirá el calendario será modificable. Aplicará la máscara de fechas de manera reactiva, según se va escribiendo. Su aspecto mientras se está escribiendo una fecha será:
	![Html Image](readme-images/inputDPMascara.png)
* Se deja comentada la posibilidad de que al perder el foco se valide que la fecha introducida es correcta, teniendo en cuenta el formato de las fechas, el número de días dependiendo del mes y los años bisiestos.